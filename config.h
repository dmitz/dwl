static const int sloppyfocus        = 1;
static const int lockfullscreen     = 1;
static const int smartgaps          = 1;
static const int bypass_surface_visibility = 1;

static const unsigned int borderpx  = 3;
static const unsigned int gappih    = 15;
static const unsigned int gappiv    = 15;
static const unsigned int gappoh    = 15;
static const unsigned int gappov    = 15;

static const float bordercolor[]    = {0.26, 0.26, 0.26, 1.0};
static const float focuscolor[]     = {0.0, 0.33, 0.46, 1.0};
static const float fullscreen_bg[]  = {0.1, 0.1, 0.1, 1.0};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* app_id    title    mask  scrkey  isflt   isterm  noswl  mon*/
	{ "foot",    NULL,    0,    0,      0,      1,      0,     -1 },
	{ NULL,      "ncm",   0,    0,      1,      1,      0,     -1 },
	{ NULL,      "pmr",   0,    0,      1,      1,      0,     -1 },
	{ NULL,      "scr",   0,   's',     1,      1,      0,     -1 },
	{ NULL,      "clc",   0,   'c',     1,      1,      0,     -1 },
};

#include "vanitygaps.c"
/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]=",     tile },
	{ "[M]",      monocle },
	{ "[F]",      NULL },
};

/* monitors */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect                x    y */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	.layout = "us,ua",
	.options = "grp:ctrl_shift_toggle,altwin:menu_win"
};

static const int repeat_rate = 50;
static const int repeat_delay = 300;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int cursor_timeout = 5;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;

static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;

static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LMR;

/* If you want to use the windows key change this to WLR_MODIFIER_LOGO */
#define MODKEY WLR_MODIFIER_LOGO
#define ALTKEY WLR_MODIFIER_ALT
#define SHIFT  WLR_MODIFIER_SHIFT
#define CTRL   WLR_MODIFIER_CTRL

#define TAGKEYS(KEY,SKEY,TAG) \
	{ MODKEY,       KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|SHIFT, SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|CTRL|SHIFT,SKEY,       toggletag,       {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* Autostart */
static const char *const autostart[] = {
		"sh", "-c", "mpd", NULL,
		"sh", "-c", "gpg-agent --daemon", NULL,
		"sh", "-c", "pipewire", NULL,
		"wbg", "/home/dmitz/.config/wall.jpg", NULL,
		"someblocks", NULL,
        NULL /* terminate */
};

/* named scratchpads - First arg only serves to match against key in rules*/
static const char *spcmd1[] = {"s", "foot", "-T", "scr", NULL };
static const char *spcmd2[] = {"c", "foot", "-T", "clc", "python", "-q", NULL };

#include "shiftview.c"
static const Key keys[] = {
	TAGKEYS(XKB_KEY_1,              XKB_KEY_1,                                0),
	TAGKEYS(XKB_KEY_2,              XKB_KEY_2,                                1),
	TAGKEYS(XKB_KEY_3,              XKB_KEY_3,                                2),
	TAGKEYS(XKB_KEY_4,              XKB_KEY_4,                                3),
	TAGKEYS(XKB_KEY_5,              XKB_KEY_5,                                4),
	TAGKEYS(XKB_KEY_6,              XKB_KEY_6,                                5),
	TAGKEYS(XKB_KEY_7,              XKB_KEY_7,                                6),
	TAGKEYS(XKB_KEY_8,              XKB_KEY_8,                                7),
	TAGKEYS(XKB_KEY_9,              XKB_KEY_9,                                8),
	{ MODKEY,                       XKB_KEY_Return,     spawn,                SHCMD("foot") },
	{ MODKEY|SHIFT,                 XKB_KEY_Return,     togglescratch,        {.v = spcmd1} },
	{ MODKEY,                       XKB_KEY_d,          spawn,                SHCMD("tofi-run") },
	{ MODKEY|SHIFT,                 XKB_KEY_d,          spawn,                SHCMD("passmenu") },
	{ MODKEY,                       XKB_KEY_w,          spawn,                SHCMD("$BROWSER") },
	{ MODKEY|SHIFT,                 XKB_KEY_w,          spawn,                SHCMD("$BROWSER --temp-basedir -s content.private_browsing true") },
	{ MODKEY,                       XKB_KEY_e,          spawn,                SHCMD("foot neomutt") },
	{ MODKEY,                       XKB_KEY_r,          spawn,                SHCMD("foot vifm") },
	{ MODKEY|SHIFT,                 XKB_KEY_r,          spawn,                SHCMD("foot btm") },
	{ MODKEY,                       XKB_KEY_m,          spawn,                SHCMD("foot -T=ncm ncmpcpp") },
	{ MODKEY|SHIFT,                 XKB_KEY_m,          spawn,                SHCMD("foot -T=pmr pulsemixer") },
	{ MODKEY,                       XKB_KEY_c,          spawn,                SHCMD("foot pipe-viewer") },
	{ MODKEY,                       XKB_KEY_n,          spawn,                SHCMD("foot newsboat") },
	{ MODKEY,                       XKB_KEY_minus,      spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-; kill -45 $(pidof someblocks)") },
	{ MODKEY|SHIFT,                 XKB_KEY_minus,      spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 15%-; kill -45 $(pidof someblocks)") },
	{ MODKEY,                       XKB_KEY_equal,      spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+; kill -45 $(pidof someblocks)") },
	{ MODKEY|SHIFT,                 XKB_KEY_equal,      spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 15%+; kill -45 $(pidof someblocks)") },
	{ MODKEY,                       XKB_KEY_BackSpace,  spawn,                SHCMD("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; kill -45 $(pidof someblocks)") },
	{ MODKEY,                       XKB_KEY_comma,      spawn,                SHCMD("mpc prev") },
	{ MODKEY|SHIFT,                 XKB_KEY_comma,      spawn,                SHCMD("mpc seek 0%") },
	{ MODKEY,                       XKB_KEY_period,     spawn,                SHCMD("mpc next") },
	{ MODKEY|SHIFT,                 XKB_KEY_period,     spawn,                SHCMD("mpc repeat") },
	{ MODKEY,                       XKB_KEY_p,          spawn,                SHCMD("mpc toggle; kill -46 $(pidof someblocks)") },
	{ SHIFT,                        XKB_KEY_Print,      spawn,                SHCMD("grimpick") },
	{ MODKEY|SHIFT,                 XKB_KEY_Print,      spawn,                SHCMD("record") },
	{ 0,                            XKB_KEY_Print,      spawn,                SHCMD("grim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ALTKEY,                       XKB_KEY_1,          spawn,                SHCMD("wlfreerdp $XDG_DATA_HOME/rdp/aws.rdp /dynamic-resolution") },
	{ ALTKEY|SHIFT,                 XKB_KEY_1,          spawn,                SHCMD("wlfreerdp $XDG_DATA_HOME/rdp/aws2.rdp /dynamic-resolution") },
	{ ALTKEY,                       XKB_KEY_2,          spawn,                SHCMD("sysact lock") },
	{ MODKEY|SHIFT,                 XKB_KEY_x,          spawn,                SHCMD("sysact shut") },
	{ MODKEY|SHIFT,                 XKB_KEY_q,          spawn,                SHCMD("sysact rest") },
	{ MODKEY|SHIFT,                 XKB_KEY_BackSpace,  spawn,                SHCMD("sysact reb") },
	{ ALTKEY,                       XKB_KEY_3,          spawn,                SHCMD("torwrap open") },
	{ ALTKEY|SHIFT,                 XKB_KEY_3,          spawn,                SHCMD("torwrap toggle") },
	{ ALTKEY,                       XKB_KEY_4,          spawn,                SHCMD("wm-mount") },
	{ ALTKEY|SHIFT,                 XKB_KEY_4,          spawn,                SHCMD("wm-umount") },
	{ 0, XKB_KEY_XF86AudioMute,                         spawn,                SHCMD("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; kill -45 $(pidof someblocks)") },
	{ 0, XKB_KEY_XF86AudioRaiseVolume,                  spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+; kill -45 $(pidof someblocks)") },
	{ 0, XKB_KEY_XF86AudioLowerVolume,                  spawn,                SHCMD("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-; kill -45 $(pidof someblocks)") },
	{ 0, XKB_KEY_XF86AudioMicMute,                      spawn,                SHCMD("wpctl set-mute @DEFAULT_SOURCE@ toggle") },
	{ MODKEY,                       XKB_KEY_t,          setlayout,            {.v = &layouts[0]} },
	{ MODKEY|SHIFT,                 XKB_KEY_t,          setlayout,            {.v = &layouts[1]} }, 
	{ MODKEY,                       XKB_KEY_y,          setlayout,            {.v = &layouts[2]} },
	{ MODKEY|SHIFT,                 XKB_KEY_y,          setlayout,            {.v = &layouts[3]} },
	{ MODKEY,                       XKB_KEY_j,          focusstack,           {.i = +1} },
	{ MODKEY|SHIFT,                 XKB_KEY_j,          movestack,            {.i = +1} },
	{ MODKEY,                       XKB_KEY_k,          focusstack,           {.i = -1} },
	{ MODKEY|SHIFT,                 XKB_KEY_k,          movestack,            {.i = -1} },
	{ MODKEY,                       XKB_KEY_v,          focusmaster,          {0} },
	{ MODKEY,                       XKB_KEY_o,          incnmaster,           {.i = +1 } },
	{ MODKEY|SHIFT,                 XKB_KEY_o,          incnmaster,           {.i = -1 } },
	{ MODKEY,                       XKB_KEY_0,          view,                 {.ui = ~0 } },
	{ MODKEY,                       XKB_KEY_q,          killclient,           {0} },
	{ MODKEY,                       XKB_KEY_a,          togglegaps,           {0} },
	{ MODKEY|SHIFT,                 XKB_KEY_a,          defaultgaps,          {0} },
	{ MODKEY,                       XKB_KEY_x,          incgaps,              {.i = +3} },
	{ MODKEY,                       XKB_KEY_z,          incgaps,              {.i = -3} },
	{ MODKEY,                       XKB_KEY_g,          shiftview,            {.i = -1} },
	{ MODKEY,                       XKB_KEY_semicolon,  shiftview,            {.i = 1} },
	{ MODKEY,                       XKB_KEY_Tab,        view,                 {0} },
	{ MODKEY,                       XKB_KEY_space,      zoom,                 {0} },
	{ MODKEY|SHIFT,                 XKB_KEY_space,      togglefloating,       {0} },
	{ MODKEY,                       XKB_KEY_h,          setmfact,             {.f = -0.05} },
	{ MODKEY,                       XKB_KEY_l,          setmfact,             {.f = +0.05} },
	{ MODKEY,                       XKB_KEY_s,          togglesticky,         {0} },
	{ MODKEY,                       XKB_KEY_f,          togglefullscreen,     {0} },
	{ MODKEY,                       XKB_KEY_apostrophe, togglescratch,        {.v = spcmd2} },
	{ MODKEY,                       XKB_KEY_b,          spawn,                SHCMD("somebar -c toggle all") },
	{ MODKEY|SHIFT,                 XKB_KEY_b,          spawn,                SHCMD("bluetooth") },
	{ MODKEY,                       XKB_KEY_Left,       focusmon,             {.i = -1 } },
	{ MODKEY|SHIFT,                 XKB_KEY_Left,       tagmon,               {.i = -1 } },
	{ MODKEY,                       XKB_KEY_Right,      focusmon,             {.i = +1 } },
	{ MODKEY|SHIFT,                 XKB_KEY_Right,      tagmon,               {.i = +1 } },
	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ CTRL|ALTKEY,XKB_KEY_Terminate_Server, quit, {0} },
#define CHVT(n) { CTRL|ALTKEY,XKB_KEY_F##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
