static void tile(Monitor *m);
static void incgaps(const Arg *arg);
static void setgaps(int oh, int ov, int ih, int iv);
static void togglegaps(const Arg *arg);
static void defaultgaps(const Arg *arg);

static int enablegaps = 1;   /* enables gaps, used by togglegaps */

void
incgaps(const Arg *arg)
{
	setgaps(
		selmon->gappoh + arg->i,
		selmon->gappov + arg->i,
		selmon->gappih + arg->i,
		selmon->gappiv + arg->i
	);
}

void
setgaps(int oh, int ov, int ih, int iv)
{
	selmon->gappoh = MAX(oh, 0);
	selmon->gappov = MAX(ov, 0);
	selmon->gappih = MAX(ih, 0);
	selmon->gappiv = MAX(iv, 0);
	arrange(selmon);
}

void
togglegaps(const Arg *arg)
{
	enablegaps = !enablegaps;
	arrange(selmon);
}

void
defaultgaps(const Arg *arg)
{
	setgaps(gappoh, gappov, gappih, gappiv);
}

void
tile(Monitor *m)
{
	unsigned int i, n = 0, h, r, oe = enablegaps, ie = enablegaps, mw, my, ty;
	Client *c;

	wl_list_for_each(c, &clients, link)
		if (VISIBLEON(c, m) && !c->isfloating && !c->isfullscreen)
			n++;
	if (n == 0)
		return;

	if (smartgaps == n) {
		oe = 0; // outer gaps disabled
	}

	if (n > m->nmaster)
		mw = m->nmaster ? (m->w.width + m->gappiv*ie) * m->mfact : 0;
	else
		mw = m->w.width - 2*m->gappov*oe + m->gappiv*ie;
	i = 0;
	my = ty = m->gappoh*oe;
	wl_list_for_each(c, &clients, link) {
		if (!VISIBLEON(c, m) || c->isfloating || c->isfullscreen)
			continue;
		if (i < m->nmaster) {
			r = MIN(n, m->nmaster) - i;
			h = (m->w.height - my - m->gappoh*oe - m->gappih*ie * (r - 1)) / r;
			resize(c, (struct wlr_box){.x = m->w.x + m->gappov*oe, .y = m->w.y + my,
				.width = mw - m->gappiv*ie, .height = h}, 0);
			my += c->geom.height + m->gappih*ie;
		} else {
			r = n - i;
			h = (m->w.height - ty - m->gappoh*oe - m->gappih*ie * (r - 1)) / r;
			resize(c, (struct wlr_box){.x = m->w.x + mw + m->gappov*oe, .y = m->w.y + ty,
				.width = m->w.width - mw - 2*m->gappov*oe, .height = h}, 0);
			ty += c->geom.height + m->gappih*ie;
		}
		i++;
	}
}
